<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $fillable = ['title', 'description', 'time'];


    // Function relation many to many (user -> meeting )
    public function users()
    {
        return $this->BelongsToMany(User::class);
    }
}
