<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    public function store(Request $request)
    {
        // Validation 
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);

        // Make Variable 
        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');

        // Make Object User
        $user = new User([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password)
        ]);

        $credentials = [
            'email' => $email,
            'password' => $password
        ];

        //Condition
        if ($user->save()) {

            $token = null;
            try{
                if(!$token = JWTAuth()->attempt($credentials)){
                    return response()->json([
                        'msg'=> 'Email or Password are incorrect',
                    ], 404);
                }
            }catch(JWTAuthException $e){
                return response()->json([
                    'msg'=> 'failed_to_created_token',
                ], 404);
            }

            $user->signin = [
                'href' => 'api/v1/user/signin',
                'method' => 'POST',
                'params' => 'email, password'
            ];
            $response = [
                'msg' => 'User Created',
                'user' => $user,
                'token'=> $token
            ];
            return response()->json($response, 201);
        }
        $response = [
            'msg' => 'An error occurred'
        ];
        return response()->json($response, 404);
    }

    public function signin(Request $request)
    {
        //
    }
}
